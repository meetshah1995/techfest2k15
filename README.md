TechFest2k15
============

Android Application Source for TF 2015.

Application Features :

MAP NAVIGATION :
View value your time and in a campus as big as 550 acres its hard to find your way to the exact event you don't want to miss. Hence we have a Google Map integration in the app with markers for all genres of events, utilities and much more which are dynamically updated.

EVENT NOTIFICATIONS: 
We know how important it is to be at the right time at a must attend event , thus we have a remind feature for you , lest you miss it.

GOOGLE DRIVE SCHEDULE SUPPORT:
No more app updates and no more large data consumption. Our Schedule is updated in real-time on the app start and cached for you.

GET DIRECTIONS:
Every event page has a Get Directions button so no more nano-searching maps to find your event path.

OPEN SOURCE:
We Share love | We share Code.

CACHE OPTIMIZATION:
The Google Maps and App essentials are stored in the cache for you to cherish later.

Have a Nice Visit !

\TF/
